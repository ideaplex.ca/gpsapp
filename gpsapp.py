from flask import Flask, g, request, render_template
from datetime import datetime


import postgresql
from contextlib import closing

app = Flask(__name__)

def connect_db():
    return postgresql.open(user = "gpsapp", database = "gpsapp", port = 5432)

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_db()
    return db


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.execute(f.read())

@app.teardown_appcontext
def teardown_request(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def direction(s):
    if s is 'N' or s is 'E':
        return 1
    else:
        return -1


@app.route('/gps', methods=['POST'])
def gps():
    input = request.data.decode('utf-8').split(',')
    lat_dir = input[4]
    lat = input[3]
    long_dir = input[6]
    long = input[5]
    date_ = input[9]
    time_ = input[1]

    latitude = direction(lat_dir) * (int(lat[0:2])+float(lat[2:])/60)
    longitude = direction(long_dir) * (int(long[0:3])+float(long[3:])/60)
    date = datetime(int(date_[4:])+2000, int(date_[2:4]), int(date_[0:2]), int(time_[0:2]), int(time_[2:4]), int(time_[4:6]))
    #print(latitude, longitude, date)
    statement = get_db().prepare("INSERT INTO positions (latitude, longitude, datetime) VALUES($1, $2, $3);")
    r = statement(latitude, longitude, date)
    print(r[1])

    return 'ok'

@app.route('/')
def index():
    results = get_db().query("SELECT id, latitude, longitude, datetime FROM positions");


    return render_template('index.html', coordinates=results)


if __name__ == '__main__':
    app.run(use_debugger=True, debug=True, use_reloader=True)
